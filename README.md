DevAO Employee API Project
Design an employee API using Express that will use basic CRUD operations.
Store your basic employee information and their related skills. 
Then use a React front-end framework with a custom reusable component to display information from one of your GET routes.

How to run it?
1. Clone this repository
2. Install dependencies using command `npm install`
3. Install MySQL server and make sure it is running on port 3306
4. Import database seed from Database folder in cloned directory
5. Run command `npm start` to run application in cloned directory

