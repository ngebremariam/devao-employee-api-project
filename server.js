// Noah Gebremariam
// Express API Project
// DevAO full stack

const express = require('express');
const bodyParser = require('body-parser');

// create express app
const app = express();

const cors = require('cors')

app.use(cors({
    origin: ['http://localhost'],
    methods: ['GET', 'PUT', 'POST', 'DELETE', 'OPTIONS'],
    allowedHeaders: ['Origin', 'Content-Type', 'Accept', 'x-access-token', 'x-access-limit', 'x-access-offset'],
    exposedHeaders: ['Origin', 'Content-Type', 'Accept', 'x-access-token', 'x-access-limit', 'x-access-offset'],
    preflightContinue: true
  }))

app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE, OPTIONS");
    res.header("Access-Control-Allow-Headers", "Origin, Accept, Content-Type, x-access-token, x-access-limit, x-access-offset");
    next();
  });

// setup the server port
const port = process.env.PORT || 3000;

// parse request data content type application/x-www-form-rulencoded
app.use(express.urlencoded({extended: false}));

// parse request data content type application/json
app.use(express.json());

// define root route
app.get('/', (req, res)=>{
    res.send('Hello World');
});

// get employee routes
const employeeRoutes = require('./src/routes/employeeroutes');

// create employee routes
app.use('/api/v1/employee/', employeeRoutes);

// listen to the port
app.listen(port, ()=>{
    console.log(`Express is running at port ${port}`);
});
