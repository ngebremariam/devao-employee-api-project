const EmployeeModel = require('../models/employeemodel');

// get all employee list
exports.getEmployeeList = (req, res)=> {
    //console.log('here all employees list');
    EmployeeModel.getAllEmployees((err, employees) =>{
        try{
            console.log('We are here');
            console.log('Employees', employees);
            res.status(200).json(employees)
        }
        catch (err) {
            res.status(400).json(err);
        }
    })
}

// read employee by ID
exports.getEmployeeByID = (req, res)=>{
    //console.log('get emp by id');
    EmployeeModel.getEmployeeByID(req.params.id, (err, employee)=>{
        try{
            console.log('single employee data',employee);
            res.status(200).json(employee);
        }
        catch (err){
            res.status(400).json(err);
        }
    })
}

// create new employee
exports.createNewEmployee = (req, res) =>{
    const employeeReqData = new EmployeeModel(req.body);
    console.log('employeeReqData', employeeReqData);
    // check null
    if(req.body.constructor === Object && Object.keys(req.body).length === 0){
        res.status(400).json({success: false, message: 'Please fill all fields'});
    }else{
        EmployeeModel.createEmployee(employeeReqData, (err, employee)=>{
            try{
               res.status(200).json({status: true, message: 'Employee Created Successfully', data: employee.insertId}) 
            }
            catch(err){
                res.status(400).json(err);
            }
        })
    }
}

// update employee
exports.updateEmployee = (req, res)=>{
    const employeeReqData = new EmployeeModel(req.body);
    console.log('employeeReqData update', employeeReqData);
    // check null
    if(req.body.constructor === Object && Object.keys(req.body).length === 0){
        res.status(400).json({success: false, message: 'Please fill all fields'});
    }else{
        EmployeeModel.updateEmployee(req.params.id, employeeReqData, (err, employee)=>{
            try{
                res.status(200).json({status: true, message: 'Employee updated Successfully'})
            }
            catch(err){
                res.status(400).json(err);
            }
        })
    }
}

// delete employee
exports.deleteEmployee = (req, res)=>{
    EmployeeModel.deleteEmployee(req.params.id, (err, employee)=>{
        try{
            res.json(200).json({success:true, message: 'Employee deleted successully!'});
        }
        catch (err){
            res.json(400).json(err);
        }
    })
}